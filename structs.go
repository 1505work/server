package main

import (
	"gopkg.in/mgo.v2/bson"
)

// Teacher - struct realizing teacher
type Teacher struct {
	ID   bson.ObjectId `bson:"_id"`
	Name string        `bson:"name" json:"name"`
	VKID int64         `bson:"vk-id" json:"vk-id"`
}

// Room - struct realizing room
type Room struct {
	ID       bson.ObjectId   `bson:"_id"`
	Number   string          `bson:"number" json:"number"`
	Teachers []bson.ObjectId `bson:"teachers" json:"teachers"`
	State    bool            `bson:"state" json:"state"`
}

//Hub - struct realizing hub
type Hub struct {
	ID   bson.ObjectId `bson:"_id"`
	Room Room          `bson:"room" json:"room"`
}
