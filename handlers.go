package main

import (
	"log"
	"net/http"
	"text/template"

	"github.com/gorilla/sessions"
	mgo "gopkg.in/mgo.v2"
)

var store = sessions.NewCookieStore([]byte(sessionStore))

var db *mgo.Database

func init() {
	session, err := mgo.Dial("mongodb://127.0.0.1")
	if err != nil {
		panic(err)
	}
	db = session.DB(dataBase)
}

//HomeHandler - home uri handler
func HomeHandler(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, sessionName)
	if auth, ok := session.Values["authenticated"].(bool); !ok || !auth {
		log.Println("not auth")
	}
	if err := template.Must(template.New("home").ParseGlob("temp/*")).ExecuteTemplate(w, "index.html", nil); err != nil {
		log.Println(err)
	}
}
