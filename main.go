package main

import (
	"flag"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

var (
	port         string
	dataBase     string
	sessionStore string
	sessionName  string
)

func init() {
	log.Println("start configuring variables...")
	flag.StringVar(&port, "port", "8080", "listening port")
	flag.StringVar(&dataBase, "db", "work1505", "mongodb db name")
	flag.StringVar(&sessionStore, "sstore", "abkchdyo9cfthanc", "session sevret key")
	flag.StringVar(&sessionName, "sname", "data", "session name")
	flag.Parse()
}

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/", HomeHandler)
	r.PathPrefix("/static/").Handler(http.FileServer(http.Dir(".")))

	http.ListenAndServe(":"+port, r)
}
